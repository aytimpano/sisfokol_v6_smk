<?php
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
/////// SISFOKOL_SMK_v6.78_(Code:Tekniknih)                     ///////
/////// (Sistem Informasi Sekolah untuk SMK)                    ///////
///////////////////////////////////////////////////////////////////////
/////// Dibuat oleh :                                           ///////
/////// Agus Muhajir, S.Kom                                     ///////
/////// URL 	:                                               ///////
///////     * http://github.com/hajirodeon                      ///////
///////     * http://gitlab.com/hajirodeon                      ///////
///////     * http://sisfokol.wordpress.com                     ///////
///////     * http://hajirodeon.wordpress.com                   ///////
///////     * http://yahoogroup.com/groups/sisfokol             ///////
///////     * https://www.youtube.com/@hajirodeon               ///////
///////////////////////////////////////////////////////////////////////
/////// E-Mail	:                                               ///////
///////     * hajirodeon@yahoo.com                              ///////
///////     * hajirodeon@gmail.com                              ///////
/////// HP/SMS/WA : 081-829-88-54                               ///////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////



session_start();


require("../../inc/config.php");
require("../../inc/fungsi.php");
require("../../inc/koneksi.php");
require("../../inc/cek/admks.php");
require("../../inc/class/paging.php");
$tpl = LoadTpl("../../template/admks.html");

nocache;

//nilai
$filenya = "jurnal.php";
$judul = "Jurnal Guru Mengajar";
$judulku = $judul;
$judulx = $judul;

$s = nosql($_REQUEST['s']);
$kunci = cegah($_REQUEST['kunci']);
$kd = nosql($_REQUEST['kd']);
$ke = $filenya;
$page = nosql($_REQUEST['page']);
if ((empty($page)) OR ($page == "0"))
	{
	$page = "1";
	}


$pegkd = cegah($_REQUEST['pegkd']);
$pegkode = cegah($_REQUEST['pegkode']);
$pegnama = cegah($_REQUEST['pegnama']);
$pkode = cegah($_REQUEST['pkode']);
$pnama = cegah($_REQUEST['pnama']);
$jkd = cegah($_REQUEST['jkd']);
$tapel = cegah($_REQUEST['tapel']);
$smt = cegah($_REQUEST['smt']);
$kelas = cegah($_REQUEST['kelas']);
$jamnya = cegah($_REQUEST['jamnya']);
$harinya = cegah($_REQUEST['harinya']);


	
	
	


//PROSES ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//reset
if ($_POST['btnRST'])
	{
	//re-direct
	xloc($filenya);
	exit();
	}





//cari
if ($_POST['btnCARI'])
	{
	//nilai
	$kunci = cegah($_POST['kunci']);


	//cek
	if (empty($kunci))
		{
		//re-direct
		$pesan = "Input Pencarian Tidak Lengkap. Harap diperhatikan...!!";
		pekem($pesan,$filenya);
		exit();
		}
	else
		{
		//re-direct
		$ke = "$filenya?kunci=$kunci";
		xloc($ke);
		exit();
		}
	}



//batal
if ($_POST['btnBTL'])
	{
	//re-direct
	xloc($filenya);
	exit();
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




//isi *START
ob_start();




//require
require("../../inc/js/jumpmenu.js");
require("../../inc/js/checkall.js");
require("../../inc/js/number.js");
require("../../inc/js/swap.js");




?>


  
  <script>
  	$(document).ready(function() {
    $('#table-responsive').dataTable( {
        "scrollX": true
    } );
} );
  </script>
  
<?php
//view //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//jika edit
if ($s == "edit")
	{
	//detailnya
	$qjuk = mysqli_query($koneksi, "SELECT * FROM guru_mengajar ".
										"WHERE tapel = '$tapel' ".
										"AND smt = '$smt' ".
										"AND kelas = '$kelas' ".
										"AND hari = '$harinya' ".
										"AND jam = '$jamnya' ".
										"AND mapel_kode = '$pkode' ".
										"AND mapel_nama = '$pnama' ".
										"AND pegawai_kd = '$pegkd' ".
										"AND pegawai_kode = '$pegkode'");
	$rjuk = mysqli_fetch_assoc($qjuk);
	$juk_postdate = balikin($rjuk['postdate']);
		
		
	echo '<a href="'.$filenya.'" class="btn btn-danger"><< LIHAT DAFTAR LAINNYA</a>
	<hr>
	
	<p>
	Tahun Pelajaran : <b>'.balikin($tapel).'</b>. 
	Semester : <b>'.balikin($smt).'</b>.
	Kelas : <b>'.balikin($kelas).'</b>.
	Hari : <b>'.balikin($harinya).'</b>.
	Jam ke-: <b>'.balikin($jamnya).'</b>. 
	</p>
	
	<p>
	Mata Pelajaran : <b>'.balikin($pnama).' [Kode:'.balikin($pkode).']</b>
	</p>
	
	
	<p>
	Guru : <b>'.balikin($pegnama).' [NIP:'.balikin($pegkode).']</b>
	</p>
	
	<hr>
	<form action="'.$filenya.'" method="post" name="formxx">
	<p>
	
	<input name="tapel" type="hidden" value="'.$tapel.'">
	<input name="smt" type="hidden" value="'.$smt.'">
	<input name="kelas" type="hidden" value="'.$kelas.'">
	<input name="harinya" type="hidden" value="'.$harinya.'">
	<input name="jamnya" type="hidden" value="'.$jamnya.'">
	<input name="pkode" type="hidden" value="'.$pkode.'">
	<input name="pnama" type="hidden" value="'.$pnama.'">
	<input name="pegkd" type="hidden" value="'.$pegkd.'">
	<input name="pegkode" type="hidden" value="'.$pegkode.'">
	<input name="pegnama" type="hidden" value="'.$pegnama.'">
	</p>
	
	</form>
	
	
	<i>Update Terakhir : <b>'.$juk_postdate.'</b></i> 
	<hr>
	
	<form action="'.$filenya.'" method="post" name="formx">';
	
	
	
	//pecah tapel
	$tapel2 = balikin($tapel);
	$tapelnya = explode("/", $tapel2);
	$tahun1 = trim($tapelnya[0]);
	$tahun2 = trim($tapelnya[1]);
	
	
	//loop bulan
	for ($k=1;$k<=12;$k++)
		{
		//jika ganjil
		if ($k <= 6)
			{
			$kk = $k+6;
			$ktahun = $tahun1;
			$smtku = 1;
			}
			
		//jika genap
		else if ($k >= 6)
			{
			$kk = $k;
			$ktahun = $tahun1;
			$smtku = 2;
			}
			
			
			
		//sesuaikan hanya semester saja
		if ($smtku == $smt)
			{
			echo "<br>
			<b>$arrbln[$kk] $ktahun</b> <br>";
			
			echo '<div class="table-responsive">          
			  <table class="table" border="1">
			    <thead>
				<tr bgcolor="'.$warnaheader.'">
			    <td width="50"><strong>TGL</strong></td>
			    <td width="50"><strong>NO</strong></td>
			    <td align="center"><strong>BAHASAN</strong></td>
			    <td align="center"><strong>HAMBATAN</strong></td>
			    <td align="center"><strong>PEMECAHAN</strong></td>
			    <td align="center" width="50"><strong>JML. ABSEN</strong></td>
			    <td align="center" width="50"><strong>JML. HADIR</strong></td>
			    <td align="center"><strong>KETERANGAN</strong></td>
				</tr>
				</thead>
		    <tbody>';
	
			//mendapatkan jumlah tanggal maksimum dalam suatu bulan
			$tgl = 0;
			$bulan = $kk;
			$bln = $kk + 1;
			$thn = $ktahun;
	
			$lastday = mktime (0,0,0,$bln,$tgl,$thn);
	
			//total tanggal dalam sebulan
			$tkhir = strftime ("%d", $lastday);
	
			//lopping tgl
			for ($i=1;$i<=$tkhir;$i++)
				{
				//ketahui harinya
				$day = $i;
				$month = $bulan;
				$year = $thn;
	
	
				//mencari hari
				$a = substr($year, 2);
					//mengambil dua digit terakhir tahun
	
				$b = (int)($a/4);
					//membagi tahun dengan 4 tanpa memperhitungkan sisa
	
				$c = $month;
					//mengambil angka bulan
	
				$d = $day;
					//mengambil tanggal
	
				$tot1 = $a + $b + $c + $d;
					//jumlah sementara, sebelum dikurangani dengan angka kunci bulan
	
				//kunci bulanan
				if ($c == 1)
					{
					$kunci = "2";
					}
	
				else if ($c == 2)
					{
					$kunci = "7";
					}
	
				else if ($c == 3)
					{
					$kunci = "1";
					}
	
				else if ($c == 4)
					{
					$kunci = "6";
					}
	
				else if ($c == 5)
					{
					$kunci = "5";
					}
	
				else if ($c == 6)
					{
					$kunci = "3";
					}
	
				else if ($c == 7)
					{
					$kunci = "2";
					}
	
				else if ($c == 8)
					{
					$kunci = "7";
					}
	
				else if ($c == 9)
					{
					$kunci = "5";
					}
	
				else if ($c == 10)
					{
					$kunci = "4";
					}
	
				else if ($c == 11)
					{
					$kunci = "2";
					}
	
				else if ($c == 12)
					{
					$kunci = "1";
					}
	
				$total = $tot1 - $kunci;
	
				//angka hari
				$hari = $total%7;
	
				//jika angka hari == 0, sebenarnya adalah 7.
				if ($hari == 0)
					{
					$hari = ($hari +7);
					}
	
				//kabisat, tahun habis dibagi empat alias tanpa sisa
				$kabisat = (int)$year % 4;
	
				if ($kabisat ==0)
					{
					$hri = $hri-1;
					}
	
	
	
				//hari ke-n
				if ($hari == 3)
					{
					$hri = 4;
					$dino = "RABU";
					}
	
				else if ($hari == 4)
					{
					$hri = 5;
					$dino = "KAMIS";
					}
	
				else if ($hari == 5)
					{
					$hri = 6;
					$dino = "JUMAT";
					}
	
				else if ($hari == 6)
					{
					$hri = 7;
					$dino = "SABTU";
					}
	
				else if ($hari == 7)
					{
					$hri = 1;
					$dino = "MINGGU";
					}
	
				else if ($hari == 1)
					{
					$hri = 2;
					$dino = "SENIN";
					}
	
				else if ($hari == 2)
					{
					$hri = 3;
					$dino = "SELASA";
					}
	
	
				//nek minggu,
				if ($hri == 1)
					{
					$warna = "red";
					$mggu_attr = "disabled";
					}
				else
					{
					if ($warna_set ==0)
						{
						$warna = $e_warna01;
						$warna_set = 1;
						$mggu_attr = "";
						}
					else
						{
						$warna = $e_warna02;
						$warna_set = 0;
						$mggu_attr = "";
						}
					}
	
	
				//cek digit
				//tgl
				if (strlen($i) == 1)
					{
					$utglx = "0$i";
					}
				else
					{
					$utglx = $i;
					}
	
	
				//bln
				if (strlen($ubln) == 1)
					{
					$ublnx = "0$ubln";
					}
				else
					{
					$ublnx = $ubln;
					}
	
	
				
				//hanya munculkan yg sesuai hari aja
				if ($dino == $harinya)
					{
					$nourut = $nourut + 1;
	
	
					//nilainya...
					$qdtf = mysqli_query($koneksi, "SELECT * FROM guru_mengajar ".
														"WHERE tapel = '$tapel' ".
														"AND smt = '$smt' ".
														"AND kelas = '$kelas' ".
														"AND hari = '$harinya' ".
														"AND jam = '$jamnya' ".
														"AND mapel_kode = '$pkode' ".
														"AND mapel_nama = '$pnama' ".
														"AND pegawai_kd = '$pegkd' ".
														"AND pegawai_kode = '$pegkode' ".
														"AND pertemuan_ke = '$nourut'");
					$rdtf = mysqli_fetch_assoc($qdtf);
					$tdtf = mysqli_num_rows($qdtf);
					$e_bahasan = balikin($rdtf['bahasan']);
					$e_hambatan = balikin($rdtf['hambatan']);
					$e_pemecahan = balikin($rdtf['pemecahan']);
					$e_jml_absen = balikin($rdtf['jml_absen']);
					$e_jml_hadir = balikin($rdtf['jml_hadir']);
					$e_ket = balikin($rdtf['ket']);
		
									
					
					echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
					echo '<td valign="top" align="center"><strong>'.$i.'</strong></td>
					<td valign="top" align="center">'.$nourut.'.</td>
					<td valign="top">'.$e_bahasan.'</td>
					<td valign="top">'.$e_hambatan.'</td>
					<td valign="top">'.$e_pemecahan.'</td>
					<td valign="top" align="right">'.$e_jml_absen.'</td>
					<td valign="top" align="right">'.$e_jml_hadir.'</td>
					<td valign="top">'.$e_ket.'</td>
					</tr>';
					}
				}
	
			echo '</tbody>
			  </table>
			  </div>';
			  }
		}

	echo '<input name="tapel" type="hidden" value="'.$tapel.'">
	<input name="smt" type="hidden" value="'.$smt.'">
	<input name="kelas" type="hidden" value="'.$kelas.'">
	<input name="harinya" type="hidden" value="'.$harinya.'">
	<input name="jamnya" type="hidden" value="'.$jamnya.'">
	<input name="pkode" type="hidden" value="'.$pkode.'">
	<input name="pnama" type="hidden" value="'.$pnama.'">
	<input name="pegkd" type="hidden" value="'.$pegkd.'">
	<input name="pegkode" type="hidden" value="'.$pegkode.'">
	<input name="pegnama" type="hidden" value="'.$pegnama.'">
	
	</form>';
	}
	
	
else
	{
	//jika null
	if (empty($kunci))
		{
		$sqlcount = "SELECT DISTINCT(pegawai_kd) AS pegkd ".
						"FROM m_mapel ".
						"WHERE pegawai_kd <> '' ".
						"ORDER BY pegawai_nama ASC, ".
						"tapel DESC";
		}
		
	else
		{
		$sqlcount = "SELECT DISTINCT(pegawai_kd) AS pegkd ".
						"FROM m_mapel ".
						"WHERE pegawai_kd <> '' ".
						"AND (tapel LIKE '%$kunci%' ".
						"OR kelas LIKE '%$kunci%' ".
						"OR jenis LIKE '%$kunci%' ".
						"OR nama LIKE '%$kunci%' ".
						"OR kode LIKE '%$kunci%' ".
						"OR kkm LIKE '%$kunci%' ".
						"OR pegawai_kode LIKE '%$kunci%' ".
						"OR pegawai_nama LIKE '%$kunci%') ".
						"ORDER BY pegawai_nama ASC, ".
						"tapel DESC";
		}
	
	
	//query
	$p = new Pager();
	$start = $p->findStart($limit);
	
	$sqlresult = $sqlcount;
	
	$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
	$pages = $p->findPages($count, $limit);
	$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
	$pagelist = $p->pageList($_GET['page'], $pages, $target);
	$data = mysqli_fetch_array($result);
	
	
	
	echo '<form action="'.$filenya.'" method="post" name="formx">
	<p>
	<input name="kunci" type="text" value="'.$kunci2.'" size="20" class="btn btn-warning" placeholder="Kata Kunci...">
	<input name="btnCARI" type="submit" value="CARI" class="btn btn-danger">
	<input name="btnBTL" type="submit" value="RESET" class="btn btn-info">
	</p>
		
	
	<div class="table-responsive">          
	<table class="table" border="1">
	<thead>
	
	<tr valign="top" bgcolor="'.$warnaheader.'">
	<td width="200"><strong><font color="'.$warnatext.'">GURU</font></strong></td>
	<td><strong><font color="'.$warnatext.'">MAPEL</font></strong></td>
	</tr>
	</thead>
	<tbody>';
	
	if ($count != 0)
		{
		do 
			{
			if ($warna_set ==0)
				{
				$warna = $warna01;
				$warna_set = 1;
				}
			else
				{
				$warna = $warna02;
				$warna_set = 0;
				}
	
			$nomer = $nomer + 1;
			$i_pegkd = nosql($data['pegkd']);
			
			//rincian
			$qyuk = mysqli_query($koneksi, "SELECT * FROM m_mapel ".
											"WHERE pegawai_kd = '$i_pegkd'");
			$ryuk = mysqli_fetch_assoc($qyuk);
			$yuk_kode = balikin($ryuk['pegawai_kode']);
			$yuk_kode2 = cegah($ryuk['pegawai_kode']);
			$yuk_nama = balikin($ryuk['pegawai_nama']);
			$yuk_nama2 = cegah($ryuk['pegawai_nama']);
			
			
	
						
			echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
			echo '<td>
			'.$yuk_nama.'
			<br>
			NIP.'.$yuk_kode.'
			</td>
			<td>';
			
			//loop
			$qyuk2 = mysqli_query($koneksi, "SELECT * FROM m_mapel ".
												"WHERE pegawai_kd = '$i_pegkd' ".
												"ORDER BY nama ASC, ".
												"tapel DESC");
			$ryuk2 = mysqli_fetch_assoc($qyuk2);
			$tyuk2 = mysqli_num_rows($qyuk2);
			
			//jika ada
			if (!empty($tyuk2))
				{
				do
					{
					//nilai
					$yuk2_pegkd = balikin($ryuk2['pegawai_kd']);
					$yuk2_kode = balikin($ryuk2['kode']);
					$yuk2_kode2 = cegah($ryuk2['kode']);
					$yuk2_nama = balikin($ryuk2['nama']);
					$yuk2_nama2 = cegah($ryuk2['nama']);
					
					echo "$yuk2_nama [$yuk2_kode]<br>";
					
					//ketahui jadwalnya
					$qyuk3 = mysqli_query($koneksi, "SELECT * FROM jadwal ".
														"WHERE mapel_kode = '$yuk2_kode' ".
														"ORDER BY tapel DESC, ".
														"smt DESC, ".
														"kelas ASC, ".
														"mapel_nama ASC, ".
														"jam_ke ASC");
					$ryuk3 = mysqli_fetch_assoc($qyuk3);
					$tyuk3 = mysqli_num_rows($qyuk3);
					
					
					//jika ada
					if (!empty($tyuk3))
						{
						echo "<ol>";
						
						do
							{
							//nilai
							$yuk3_kd = balikin($ryuk3['kd']);
							$yuk3_tapel = balikin($ryuk3['tapel']);
							$yuk3_tapel2 = cegah($ryuk3['tapel']);
							$yuk3_smt = balikin($ryuk3['smt']);
							$yuk3_smt2 = cegah($ryuk3['smt']);
							$yuk3_kelas = balikin($ryuk3['kelas']);
							$yuk3_kelas2 = cegah($ryuk3['kelas']);
							$yuk3_jam = balikin($ryuk3['jam_ke']);
							$yuk3_jam2 = cegah($ryuk3['jam_ke']);
							$yuk3_hari = balikin($ryuk3['hari']);
							$yuk3_hari2 = cegah($ryuk3['hari']);

							
							//detailnya
							$qjuk = mysqli_query($koneksi, "SELECT * FROM guru_mengajar ".
																"WHERE tapel = '$yuk3_tapel2' ".
																"AND smt = '$yuk3_smt2' ".
																"AND kelas = '$yuk3_kelas2' ".
																"AND hari = '$yuk3_hari2' ".
																"AND jam = '$yuk3_jam2' ".
																"AND mapel_kode = '$yuk2_kode2' ".
																"AND mapel_nama = '$yuk2_nama2' ".
																"AND pegawai_kode = '$yuk_kode2'");
							$rjuk = mysqli_fetch_assoc($qjuk);
							$juk_postdate = balikin($rjuk['postdate']);
													
							
														
							echo "<li>
							TAPEL:<b>$yuk3_tapel</b>. SMT:<b>$yuk3_smt</b>. Kelas:<b>$yuk3_kelas</b>. Hari:<b>$yuk3_hari</b>. Jam ke-<b>$yuk3_jam</b>
							<br>
							<a href='$filenya?s=edit&pegkd=$yuk2_pegkd&pegkode=$yuk_kode2&pegnama=$yuk_nama2&pkode=$yuk2_kode2&pnama=$yuk2_nama2&jkd=$yuk3_kd&tapel=$yuk3_tapel2&smt=$yuk3_smt2&kelas=$yuk3_kelas2&jamnya=$yuk3_jam2&harinya=$yuk3_hari2' class='btn btn-danger'>LIHAT JURNAL >></a>
							[Update Terakhir : $juk_postdate].
							<br>
							<br>
							</li>";
							}
						while ($ryuk3 = mysqli_fetch_assoc($qyuk3));
						
						echo "</ol>";
						}
					else
						{
						echo "<p>
						<b><font color='red'>Belum Punya Jadwal.</font></b>
						</p>";
						}
	
					
													
					echo "<hr>";				
					}
				while ($ryuk2 = mysqli_fetch_assoc($qyuk2));
				}
			
			echo '</td>
	        </tr>';
			
			}
		while ($data = mysqli_fetch_assoc($result));
		}
	
	
	echo '</tbody>
	  </table>
	  </div>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="3">
	<tr>
	<td>
	<strong><font color="#FF0000">'.$count.'</font></strong> Data. '.$pagelist.'
	<br>
	
	<input name="jml" type="hidden" value="'.$count.'">
	<input name="s" type="hidden" value="'.$s.'">
	<input name="kd" type="hidden" value="'.$kdx.'">
	<input name="page" type="hidden" value="'.$page.'">
	
	</td>
	</tr>
	</table>
	</form>';
	}



//isi
$isi = ob_get_contents();
ob_end_clean();

require("../../inc/niltpl.php");


//diskonek
xfree($qbw);
xclose($koneksi);
exit();
?>